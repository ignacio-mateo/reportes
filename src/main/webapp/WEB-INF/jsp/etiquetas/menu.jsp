<form method="post" id="frmMenu">
	
	<div id="menu">
		<div id="wrapper">
			<ul class="menu">
			
				<!-- MENU ARCHIVOS --> 
				<li class="item1"><a href="#">ARCHIVOS</a>
					<ul>
						<li class="subitem1"><a href="#" onclick="seleccionarOpcion('cargaArchivoInicio');">Subir Archivo</a></li>
						<li class="subitem2"><a href="#" onclick="seleccionarOpcion('bitacoraInicio');">Bitacora</a></li>
						<li class="subitem3"><a href="#" onclick="seleccionarOpcion('generaReporteInicio');">Descargar Archivo</a></li>
					</ul>
				</li>
												
				<!-- MENU USUARIOS -->
				<li class="item2"><a href="#">USUARIO</a>
				    <ul>
						<li class="subitem1"><a href="#" onclick="seleccionarOpcion('agregarUsuarioInicio');">Agregar Usuario</a></li>
						<li class="subitem2"><a href="#" onclick="seleccionarOpcion('actualizarUsuarioInicio');">Actualizar Usuario</a></li>
					</ul>
				</li>

				
				<!-- MENU SALIR --> 
				<li class="item1"><a href="#">SALIR</a>
					<ul>
						<li class="subitem1"><a href="<%= request.getContextPath()%>/">Cerrar Session</a></li>
					</ul>
				</li>
				
			</ul>
		</div>
	</div>
	
</form>