<html>
<head>
	<%@include file="/WEB-INF/jsp/etiquetas/tagslibs.jsp"%>
	<%@include file="/WEB-INF/jsp/etiquetas/tagscss.jsp"%>
</head>

<body>
	<%@include file="/WEB-INF/jsp/etiquetas/menu.jsp"%>
	<div class="cuerpo">
		<table class="tablaPrincipal">
			<tr>
				<td><input type="text" class="columna1" value="REGISTRO" readonly="readonly"></td>
				<td><input type="text" class="columna2" value="SERIE" readonly="readonly"></td>
				<td><input type="text" class="columna1" value="REGION" readonly="readonly"></td>
				<td><input type="text" class="columna3" value="DESCRIPCION" readonly="readonly"></td>
			</tr>
			<c:forEach items="${listaBitacora}" var="bitacoraTO" varStatus="status">
			<tr id="fila${status.index}">
				<td><input type="text" class="columna1" value="${status.index+1}" readonly="readonly"></td>
				<td><input type="text" class="columna2" value="${bitacoraTO.idBitacora}" readonly="readonly"></td>
				<td><input type="text" class="columna1" value="${bitacoraTO.idRegion}" readonly="readonly"></td>
				<td><input type="text" class="columna3" value="${bitacoraTO.descripcion}" readonly="readonly"></td>
			</tr>	
			</c:forEach>
		</table>
		<div>&nbsp;</div>
		<div>&nbsp;</div>
	</div>
</body>

</html>