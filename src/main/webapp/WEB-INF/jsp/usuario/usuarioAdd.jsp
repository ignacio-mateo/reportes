<html>
<head>
	<%@include file="/WEB-INF/jsp/etiquetas/tagslibs.jsp"%>
	<%@include file="/WEB-INF/jsp/etiquetas/tagscss.jsp"%>
</head>

<body>
    <%@include file="/WEB-INF/jsp/etiquetas/menu.jsp"%>
	<div class="cuerpo">
		<form id="frmUsuario" method="post" action="agregarUsuario">
			
			<table class="tablaPrincipal">
				<tr>
					<td colspan="2">Alta de Usuarios</td>
				</tr>
				<tr><td colspan="2">&nbsp;</td></tr>
				<tr>
					<td>Num. Emp:</td>
					<td><input type="text" id="user" name="user"></td>
				</tr>
				<tr><td colspan="2">&nbsp;</td></tr>
				<tr>
					<td colspan="2">
						<div class="mensaje">${usuarioTO.mensajeAut}</div>
					</td>
				</tr>
				<tr><td colspan="2">&nbsp;</td></tr>
				<tr>
					<td>
						<input type="submit" id="agregar" value="Agregar">
					</td>
				</tr>
			</table>
		</form>
	</div>
	
</body>

</html>
