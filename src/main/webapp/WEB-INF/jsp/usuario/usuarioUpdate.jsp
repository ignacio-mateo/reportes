<html>
<head>
	<%@include file="/WEB-INF/jsp/etiquetas/tagslibs.jsp"%>
	<%@include file="/WEB-INF/jsp/etiquetas/tagscss.jsp"%>
</head>

<body>
	<%@include file="/WEB-INF/jsp/etiquetas/menu.jsp"%>	
	
	<div class="cuerpo">
		<form id="frmUsuario" method="POST" action="actualizarUsuario">
			
			<table class="tablaPrincipal">
				<tr>
					<td>&nbsp;</td>
					<td colspan="2">Actualizacion de Usuarios</td>
					<td>&nbsp;</td>
				</tr>
				<tr><td colspan="4">&nbsp;</td></tr>
				<tr>
					<td>&nbsp;</td>
					<td>Num. Emp:</td>
					<td><input type="text" name="id"></td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>Perfil: </td>
					<td>
						<select name="idPf">>
							<option value="0" selected="selected">Seleccione Opcion</option>
							<option value="1">Administrador</option>
							<option value="2">Usuario</option>
						</select>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>Activo: </td>
					<td>
						<select name="idEst">>
							<option value="0" selected="selected">Seleccione Opcion</option>
							<option value="1">Activo</option>
							<option value="2">Desactivar</option>
						</select>
					<td>&nbsp;</td>
				</tr>
				<tr><td colspan="4">&nbsp;</td></tr>
				<tr><td colspan="4">&nbsp;</td></tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>
						<input type="submit" id="actualizar" value="Actualizar">
					</td>
					<td>&nbsp;</td>
				</tr>
				<tr><td colspan="4">&nbsp;</td></tr>
				<tr><td colspan="4">&nbsp;</td></tr>
				<tr>
					<td><input type="text" value="EMPLEADO" readonly="readonly"></td>
					<td><input type="text" value="PERFIL" readonly="readonly"></td>
					<td><input type="text" value="ESTATUS" readonly="readonly"></td>
					<td><input type="text" class="columnaNombre" value="NOMBRE" readonly="readonly"></td>
				</tr>
				<c:forEach items="${listaUsuario}" var="usuarioTO" varStatus="status">
				<tr id="fila${status.index}">
					<td><input type="text" value="${usuarioTO.idUsuario}" readonly="readonly"></td>
					<td><input type="text" value="${usuarioTO.perfil}" readonly="readonly"></td>
					<td><input type="text" value="${usuarioTO.estatus}" readonly="readonly"></td>
					<td><input type="text" class="columnaNombre" value="${usuarioTO.nombre} ${usuarioTO.apellidoP} ${usuarioTO.apellidoM}" readonly="readonly"></td>
				</tr>	
				</c:forEach>
			</table>
		</form>
	</div>
	
</body>

</html>
