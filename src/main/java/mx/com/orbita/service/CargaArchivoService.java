package mx.com.orbita.service;

import java.io.InputStream;

public interface CargaArchivoService {

	Integer cargaArchivoXls(InputStream input, Integer region);
	Integer cargaArchivoXlsx(InputStream input, Integer region);

}
