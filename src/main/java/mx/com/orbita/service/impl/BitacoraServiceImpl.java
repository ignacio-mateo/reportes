package mx.com.orbita.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import mx.com.orbita.dao.BitacoraDao;
import mx.com.orbita.service.BitacoraService;
import mx.com.orbita.to.BitacoraTO;

@Component
public class BitacoraServiceImpl implements BitacoraService {

	private BitacoraDao bitacoraDao;

	public List<BitacoraTO> getBitacora(Integer idRegion){
		return bitacoraDao.getBitacora(idRegion);
	}

	@Autowired
	public void setBitacoraDao(BitacoraDao bitacoraDao) {
		this.bitacoraDao = bitacoraDao;
	}

}
