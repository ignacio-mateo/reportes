package mx.com.orbita.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import mx.com.orbita.dao.GeneraReporteDao;
import mx.com.orbita.service.GeneraReporteService;
import mx.com.orbita.to.ReporteTO;

@Component
public class GeneraReporteServiceImpl implements GeneraReporteService{

	private GeneraReporteDao generaReporteDao;

	public List<ReporteTO> getReporte(Integer idRegion) {
		return generaReporteDao.getReporte(idRegion);
	}

	@Autowired
	public void setGeneraReporteDao(GeneraReporteDao generaReporteDao) {
		this.generaReporteDao = generaReporteDao;
	}

}
