package mx.com.orbita.service.impl;

import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import mx.com.orbita.dao.UsuarioDao;
import mx.com.orbita.service.UsuarioService;
import mx.com.orbita.to.UsuarioTO;
import mx.com.orbita.util.Codigo;

@Component
public class UsuarioServiceImpl implements UsuarioService {

	private UsuarioDao usuarioDao;

  	private static final Logger logger = Logger.getLogger(UsuarioServiceImpl.class);

  	public UsuarioTO getAutenticar(Integer id, String llave){
  		UsuarioTO usuarioTO = new UsuarioTO();
  		// Consultar si el usuario esta activo
		UsuarioTO usuario = usuarioDao.getUsuario(id);
		if(usuario.getIdEstatus().equals(2)){
			usuarioTO.setIdAut(Codigo.ID_USUARIO_INACTIVO);
			usuarioTO.setMensajeAut(Codigo.MSG_USUARIO_INACTIVO);
		}
		logger.info(" activacion ["+usuarioTO.getIdAut()
				+"] estatusUsuario ["+usuario.getIdEstatus()
				+"] mensaje ["+usuarioTO.getMensajeAut()+"]");

  		return usuarioTO;
  	}

  	public UsuarioTO setUsuario(Integer id){
  		UsuarioTO usuarioTO = new UsuarioTO();
  		usuarioTO.setIdUsuario(id);
  		usuarioTO.setIdPerfil(Codigo.ID_PERFIL_USR);
  		usuarioTO.setIdEstatus(Codigo.ID_USUARIO_ACTIVO);
  		usuarioDao.setUsuario(usuarioTO);
  		return usuarioTO;
  	}

  	public List<UsuarioTO> getUsuario(){
  		return usuarioDao.getUsuario();
  	}

  	public Integer updateUsuario(UsuarioTO usuarioTO){
  		return usuarioDao.updateUsuario(usuarioTO);
  	}

  	@Autowired
	public void setUsuarioDao(UsuarioDao usuarioDao) {
		this.usuarioDao = usuarioDao;
	}

}
