package mx.com.orbita.service.impl;

import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Component;
import mx.com.orbita.dao.CargaArchivoDao;
import mx.com.orbita.service.CargaArchivoService;

@Component
public class CargaArchivoServiceImpl implements CargaArchivoService {

	private static final Logger logger = Logger.getLogger(CargaArchivoServiceImpl.class);

	private CargaArchivoDao cargaArchivoDao;

	public Integer cargaArchivoXls(InputStream input, Integer region){
		Integer resultado = 0;
		try {
			// Crear el objeto que tendra el libro de Excel
			HSSFWorkbook libro = new HSSFWorkbook(input);

			/*
			 * Obtenemos la primera pestania a la que se quiera procesar indicando el indice.
			 * Una vez obtenida la hoja excel con las filas que se quieren leer obtenemos el iterator
			 * que nos permite recorrer cada una de las filas que contiene.
			 */
			HSSFSheet hoja = libro.getSheetAt(0);
			Iterator<Row> rowIterator = hoja.iterator();
			eliminarRegistros(region);
			insertarSeries(rowIterator,region);
			
			// cerramos el libro excel
			libro.close();
		} catch (IOException e) {
			logger.info("No es posible cerrar el objeto HSSFWorkbook: "+e.getMessage());
		}
		return resultado;
	}
	
	public Integer cargaArchivoXlsx(InputStream input, Integer region) {
		Integer resultado = 0;
		try {
			// Crear el objeto que tendra el libro de Excel
			XSSFWorkbook libro = new XSSFWorkbook(input);
			
			/*
			 * Obtenemos la primera pestania a la que se quiera procesar indicando el indice.
			 * Una vez obtenida la hoja excel con las filas que se quieren leer obtenemos el iterator
			 * que nos permite recorrer cada una de las filas que contiene.
			 */
			XSSFSheet hoja = libro.getSheetAt(0);
			Iterator<Row> rowIterator = hoja.iterator();
			eliminarRegistros(region);
			insertarSeries(rowIterator,region);
			
			// cerramos el libro excel
			libro.close();
		} catch (IOException e) {
			logger.info("No es posible cerrar el objeto HSSFWorkbook: "+e.getMessage());
		}
		return resultado;
	}
	
	private void insertarSeries(Iterator<Row> rowIterator, Integer region){
		Long serie = 0L;
		String tipo = "";
		Row fila;
		// Recorremos todas las filas para mostrar el contenido de cada celda
		while (rowIterator.hasNext()){
			fila = rowIterator.next();

			// Obtenemos el iterator que permite recorres todas las celdas de una fila
			Iterator<Cell> cellIterator = fila.cellIterator();
			Cell celda;

			while (cellIterator.hasNext()){
				celda = cellIterator.next();
				logger.info(" fila: ["+fila.getRowNum()+"] celda: ["+celda.getRowIndex()+"]");

				// Dependiendo del formato de la celda el valor se debe mostrar como String, Fecha, boolean, entero...
				switch(celda.getCellType()) {
					case Cell.CELL_TYPE_NUMERIC:
						if( HSSFDateUtil.isCellDateFormatted(celda) ){
							//logger.info(celda.getDateCellValue());
						}else{
							//logger.info(celda.getNumericCellValue());
							Double num = celda.getNumericCellValue();
							Long numLong = num.longValue();
							serie = numLong;
							tipo = "NUMERIC";
						}
						break;
					case Cell.CELL_TYPE_STRING:
						//logger.info(celda.getStringCellValue());
						try {
							serie = Long.parseLong(celda.getStringCellValue());
						} catch (Exception e) {
							try {
								Integer operacion = cargaArchivoDao.registraBitacora(celda.getStringCellValue(), region, "No es numerico la serie");
								logger.info("fila: ["+fila.getRowNum()+"] serie: ["+serie+"] region: ["+region+"] resultado bitacora1: ["+operacion+"]");
							} catch (DuplicateKeyException e2) {
								// Quitar este cath cuando se mande el folio sisap
								logger.info(" Serie duplicada en la Tabla Bitacora por el unique de idserie");
							}
						}
						tipo = "STRING";
						break;
					case Cell.CELL_TYPE_BOOLEAN:
						//logger.info(celda.getBooleanCellValue());
						break;
				}// Fin del switch
				
				try {
					Integer operacion = cargaArchivoDao.cargaArchivo(serie,region);
					logger.info("fila: ["+fila.getRowNum()+"] serie: ["+serie+"] region: ["+region+"] tipo: ["+tipo+"] resultado insert: ["+operacion+"]");
				} catch (DuplicateKeyException e) {
					try {
						Integer operacion = cargaArchivoDao.registraBitacora(serie.toString(), region, "Serie duplicada");
						logger.info("fila: ["+fila.getRowNum()+"] serie: ["+serie+"] region: ["+region+"] resultado bitacora2: ["+operacion+"]");
					} catch (DuplicateKeyException e2) {
						// Quitar este cath cuando se mande el folio sisap
						logger.info(" Serie duplicada en la Tabla Bitacora por el unique de idserie");
					}				
				}
			}// Fin del while
		}// Fin del while
	}
	
	public void eliminarRegistros(Integer region){
		logger.info("eliminar registros de region: ["+region+"]");
		cargaArchivoDao.borrarSerie(region);
		cargaArchivoDao.borrarBitacora(region);
		logger.info("se ha terminado de borrar los registros");
	}


	@Autowired
	public void setCargaArchivoDao(CargaArchivoDao cargaArchivoDao) {
		this.cargaArchivoDao = cargaArchivoDao;
	}

}
