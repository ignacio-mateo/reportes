package mx.com.orbita.service;

import java.util.List;
import mx.com.orbita.to.ReporteTO;

public interface GeneraReporteService {

	List<ReporteTO> getReporte(Integer idRegion);

}
