package mx.com.orbita.service;

import java.util.List;
import mx.com.orbita.to.UsuarioTO;

public interface UsuarioService {

	UsuarioTO getAutenticar(Integer id, String llave);
	UsuarioTO setUsuario(Integer id);
	List<UsuarioTO> getUsuario();
	Integer updateUsuario(UsuarioTO usuarioTO);

}
