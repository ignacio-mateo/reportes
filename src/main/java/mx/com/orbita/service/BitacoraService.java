package mx.com.orbita.service;

import java.util.List;
import mx.com.orbita.to.BitacoraTO;

public interface BitacoraService {

	List<BitacoraTO> getBitacora(Integer idRegion);

}
