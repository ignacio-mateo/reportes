package mx.com.orbita.configuracion;

import javax.sql.DataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.lookup.JndiDataSourceLookup;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import mx.com.orbita.dao.BitacoraDao;
import mx.com.orbita.dao.CargaArchivoDao;
import mx.com.orbita.dao.GeneraReporteDao;
import mx.com.orbita.dao.UsuarioDao;
import mx.com.orbita.dao.impl.BitacoraDaoImpl;
import mx.com.orbita.dao.impl.CargaArchivoDaoImpl;
import mx.com.orbita.dao.impl.GeneraReporteDaoImpl;
import mx.com.orbita.dao.impl.UsuarioDaoImpl;


@Configuration
@ComponentScan(basePackages={"mx.com.orbita"},
	excludeFilters={
		@Filter(type=FilterType.ANNOTATION, value=EnableWebMvc.class)
	})
public class RootConfig {

	/**
	 * Configuracion de DataSource
	 *
	 */
	@Bean
    public DataSource dataSource() {
        final JndiDataSourceLookup dsLookup = new JndiDataSourceLookup();
        dsLookup.setResourceRef(true);
        DataSource dataSource = dsLookup.getDataSource("java:comp/env/jdbc/reportes");
        return dataSource;
    }

	/**
	 * Configuracion de JdbcTemplate
	 *
	 */
	@Bean
	public JdbcTemplate jdbcTemplate(){
		JdbcTemplate jdbcTemplate = new JdbcTemplate();
		jdbcTemplate.setDataSource(dataSource());
		return jdbcTemplate;
	}

	/**
	 * Configuracion de DAO's
	 *
	 */
	@Bean
	public UsuarioDao usuarioDao(){
		UsuarioDaoImpl usuarioDaoImpl = new UsuarioDaoImpl();
		usuarioDaoImpl.setJdbcTemplate(jdbcTemplate());
		return usuarioDaoImpl;
	}

	@Bean
	public GeneraReporteDao generaReporteDao(){
		GeneraReporteDaoImpl generaReporteDaoImpl = new GeneraReporteDaoImpl();
		generaReporteDaoImpl.setJdbcTemplate(jdbcTemplate());
		return generaReporteDaoImpl;
	}

	@Bean
	public CargaArchivoDao cargaArchivoDao(){
		CargaArchivoDaoImpl cargaArchivoDaoImpl = new CargaArchivoDaoImpl();
		cargaArchivoDaoImpl.setJdbcTemplate(jdbcTemplate());
		return cargaArchivoDaoImpl;
	}

	@Bean
	public BitacoraDao bitacoraDao(){
		BitacoraDaoImpl bitacoraDaoImpl = new BitacoraDaoImpl();
		bitacoraDaoImpl.setJdbcTemplate(jdbcTemplate());
		return bitacoraDaoImpl;
	}

}