package mx.com.orbita.to;

public class BitacoraTO {

	private String idBitacora;
	private String idRegion;
	private String descripcion;

	public String getIdBitacora() {
		return idBitacora;
	}
	public void setIdBitacora(String idBitacora) {
		this.idBitacora = idBitacora;
	}
	public String getIdRegion() {
		return idRegion;
	}
	public void setIdRegion(String idRegion) {
		this.idRegion = idRegion;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

}
