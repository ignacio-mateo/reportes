package mx.com.orbita.to;

public class UsuarioTO {

	private Integer idUsuario;
	private String nombre;
	private String apellidoP;
	private String apellidoM;
	private Integer idPerfil;
	private String  perfil;
	private Integer idEstatus;
	private String estatus;
	private Integer op;
	private Integer idAut;
	private String mensajeAut;

	public Integer getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellidoP() {
		return apellidoP;
	}
	public void setApellidoP(String apellidoP) {
		this.apellidoP = apellidoP;
	}
	public String getApellidoM() {
		return apellidoM;
	}
	public void setApellidoM(String apellidoM) {
		this.apellidoM = apellidoM;
	}
	public Integer getIdPerfil() {
		return idPerfil;
	}
	public void setIdPerfil(Integer idPerfil) {
		this.idPerfil = idPerfil;
	}
	public String getPerfil() {
		return perfil;
	}
	public void setPerfil(String perfil) {
		this.perfil = perfil;
	}
	public Integer getIdEstatus() {
		return idEstatus;
	}
	public void setIdEstatus(Integer idEstatus) {
		this.idEstatus = idEstatus;
	}
	public String getEstatus() {
		return estatus;
	}
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	public Integer getOp() {
		return op;
	}
	public void setOp(Integer op) {
		this.op = op;
	}
	public Integer getIdAut() {
		return idAut;
	}
	public void setIdAut(Integer idAut) {
		this.idAut = idAut;
	}
	public String getMensajeAut() {
		return mensajeAut;
	}
	public void setMensajeAut(String mensajeAut) {
		this.mensajeAut = mensajeAut;
	}

}
