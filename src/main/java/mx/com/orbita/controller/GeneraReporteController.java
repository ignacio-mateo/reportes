package mx.com.orbita.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import mx.com.orbita.service.GeneraReporteService;
import mx.com.orbita.to.ReporteTO;
import mx.com.orbita.to.UsuarioTO;

@Controller
public class GeneraReporteController {

	private static final Logger logger = Logger.getLogger(CargaArchivoController.class);

	private GeneraReporteService generaReporteService;

	@RequestMapping(value="generaReporteInicio")
	protected ModelAndView inicioReporte(UsuarioTO usuarioTO){
		logger.info("CargaArchivoController.login");
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("usuarioTO",usuarioTO);
		modelAndView.setViewName("generaArchivo/generaArchivoInicio");
		return modelAndView;
	}

	@RequestMapping(value="generaReporte")
	protected ModelAndView getReporte(HttpServletResponse response,UsuarioTO usuarioTO){
		logger.info("ReporteController.getReporteCategoria");
		ModelAndView modelAndView = new ModelAndView();
		List<ReporteTO> lista = generaReporteService.getReporte(usuarioTO.getOp());
		modelAndView.addObject("lista", lista);
		response.setHeader("Content-type","application/vnd.ms-excel");
	    response.setHeader("Content-Disposition","attachment; filename=\"reporteSigadeGT04.csv\"");
	    modelAndView.addObject("usuarioTO",usuarioTO);
	    modelAndView.setViewName("reporteExcelView");
		return modelAndView;
	}

	@Autowired
	public void setGeneraReporteService(GeneraReporteService generaReporteService) {
		this.generaReporteService = generaReporteService;
	}

}
