package mx.com.orbita.controller;

import java.util.List;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import mx.com.orbita.service.UsuarioService;
import mx.com.orbita.to.UsuarioTO;
import mx.com.orbita.util.Codigo;
import org.apache.log4j.Logger;

@Controller
public class UsuarioController {

private static final Logger logger = Logger.getLogger(UsuarioController.class);

	private UsuarioService usuarioService;

	@RequestMapping(value="/inicio", method = RequestMethod.POST)
	protected ModelAndView login(@RequestParam Integer user, @RequestParam String llave){
		logger.info("UsuarioController.inicio");
		logger.info("  Parametros de Peticion [idEmp: "+user+" llave: "+llave+"]");
		ModelAndView modelAndView = new ModelAndView();

		UsuarioTO usuarioTO = usuarioService.getAutenticar(user,llave);
		logger.info("  Respuesta [idEmp: "+usuarioTO.getIdUsuario()
											+" nombre: "+usuarioTO.getNombre()+" "
											+usuarioTO.getApellidoP()+" "
											+usuarioTO.getApellidoM()+"]");

		modelAndView.addObject("usuarioTO",usuarioTO);
		modelAndView.setViewName("inicio/inicio");
		logger.info("  * Fin Peticion");
		return modelAndView;
	}

	@RequestMapping(value="agregarUsuarioInicio")
	protected ModelAndView agregarUsuarioInicio(UsuarioTO usuarioTO){
		logger.info("UsuarioController.usuarioAdd");
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("usuarioTO",usuarioTO);
		modelAndView.setViewName("usuario/usuarioAdd");
		return modelAndView;
	}

	@RequestMapping(value="agregarUsuario")
	protected ModelAndView agregarUsuario(UsuarioTO usuarioTO,@RequestParam Integer user){
		logger.info("UsuarioController.setUsuario");
		ModelAndView modelAndView = new ModelAndView();
		UsuarioTO usuario = usuarioService.setUsuario(user);
		if(!usuario.getIdUsuario().equals(0)){
			usuarioTO.setMensajeAut(Codigo.MSG_USUARIO_AGREGADO);
			modelAndView.setViewName("usuario/usuarioExito");
		}else{
			usuarioTO.setMensajeAut(usuario.getMensajeAut());
			modelAndView.setViewName("usuario/usuarioAdd");
		}
		modelAndView.addObject("usuarioTO",usuarioTO);
		return modelAndView;
	}

	@RequestMapping(value="actualizarUsuarioInicio")
	protected ModelAndView actualizarUsuarioInicio(UsuarioTO usuarioTO){
		logger.info("UsuarioController.usuarioUpdate");
		ModelAndView modelAndView = new ModelAndView();
		List<UsuarioTO> listaUsuario = usuarioService.getUsuario();
		modelAndView.addObject("listaUsuario",listaUsuario);
		modelAndView.addObject("usuarioTO",usuarioTO);
		modelAndView.setViewName("usuario/usuarioUpdate");
		return modelAndView;
	}

	@RequestMapping(value="actualizarUsuario")
	protected ModelAndView actualizarUsuario(UsuarioTO usuarioTO,
												@RequestParam Integer id,
												@RequestParam Integer idPf,
												@RequestParam Integer idEst){
		logger.info("UsuarioController.updateUsuario");
		ModelAndView modelAndView = new ModelAndView();
		UsuarioTO usuario = new UsuarioTO();
		usuario.setIdUsuario(id);
		usuario.setIdPerfil(idPf);
		usuario.setIdEstatus(idEst);
		usuarioService.updateUsuario(usuario);
		usuarioTO.setMensajeAut(Codigo.MSG_USUARIO_ACTUALIZADO);
		modelAndView.addObject("usuarioTO",usuarioTO);
		modelAndView.setViewName("usuario/usuarioExito");
		return modelAndView;
	}

	@Autowired
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}

}
