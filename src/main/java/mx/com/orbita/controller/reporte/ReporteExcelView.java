package mx.com.orbita.controller.reporte;

import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.util.CellRangeAddress;
import org.springframework.web.servlet.view.document.AbstractExcelView;
import mx.com.orbita.to.ReporteTO;

public class ReporteExcelView extends AbstractExcelView{

	public static final Logger logger = Logger.getLogger(ReporteExcelView.class);

	@SuppressWarnings("unchecked")
	@Override
	protected void buildExcelDocument(Map<String, Object> model, HSSFWorkbook workbook,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		logger.info("CategoriaExcelView.buildExcelDocument");
		List<ReporteTO> lista = (List<ReporteTO>) model.get("lista");
		generaReporte(workbook, lista);
	}

	/**
	 * @param workbook
	 * @param lista
	 */
	private void generaReporte(HSSFWorkbook workbook, List<ReporteTO> lista){
		logger.info("ReporteExcelView.generaReporteCategoria");

	    // Crear la hoja
        HSSFSheet sheet = workbook.createSheet("Reporte");
        sheet.setDefaultColumnWidth(15);

        // Crear el estilo de las celdas
        CellStyle style = workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setFontName("Arial");
        style.setFillForegroundColor(HSSFColor.DARK_BLUE.index);
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        font.setColor(HSSFColor.WHITE.index);
        style.setFont(font);

        // Crear Filas
        HSSFRow celda = sheet.createRow(1);
        celda.createCell(0).setCellValue("CAMPO 1");
        celda.getCell(0).setCellStyle(style);
        celda.createCell(1).setCellValue("CAMPO 2");
        celda.getCell(1).setCellStyle(style);

        int rowCount = 2;

        for (ReporteTO reporteTO : lista) {
        	HSSFRow datos = sheet.createRow(rowCount++);
        	datos.createCell(0).setCellValue(reporteTO.getIdSerie());
        	datos.createCell(1).setCellValue(reporteTO.getIdRegion());
		}
	}

} // Fin de la clase
