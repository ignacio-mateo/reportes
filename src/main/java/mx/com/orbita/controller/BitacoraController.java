package mx.com.orbita.controller;

import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import mx.com.orbita.service.BitacoraService;
import mx.com.orbita.to.BitacoraTO;
import mx.com.orbita.to.UsuarioTO;

@Controller
public class BitacoraController{

private static final Logger logger = Logger.getLogger(BitacoraController.class);

	private BitacoraService bitacoraService;

	@RequestMapping(value="bitacoraInicio")
	protected ModelAndView inicio(UsuarioTO usuarioTO){
		logger.info("BitacoraController.inicio");
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("usuarioTO",usuarioTO);
		modelAndView.setViewName("bitacora/bitacoraInicio");
		return modelAndView;
	}

	@RequestMapping(value="bitacoraRegistros")
	protected ModelAndView getBitacora(UsuarioTO usuarioTO){
		logger.info("BitacoraController.inicio");
		ModelAndView modelAndView = new ModelAndView();
		List<BitacoraTO> listaBitacora = bitacoraService.getBitacora(usuarioTO.getOp());
		modelAndView.addObject("listaBitacora",listaBitacora);
		modelAndView.addObject("usuarioTO",usuarioTO);
		modelAndView.setViewName("bitacora/bitacoraRegistro");
		return modelAndView;
	}

	@Autowired
	public void setBitacoraService(BitacoraService bitacoraService) {
		this.bitacoraService = bitacoraService;
	}

}
