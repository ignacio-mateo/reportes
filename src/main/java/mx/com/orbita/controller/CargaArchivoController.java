package mx.com.orbita.controller;

import java.io.IOException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import mx.com.orbita.service.CargaArchivoService;
import mx.com.orbita.to.UsuarioTO;

@Controller
public class CargaArchivoController {

	private static final Logger logger = Logger.getLogger(CargaArchivoController.class);

	private CargaArchivoService cargaArchivoService;

	@RequestMapping(value="cargaArchivoInicio")
	protected ModelAndView login(UsuarioTO usuarioTO){
		logger.info("CargaArchivoController.login");
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("usuarioTO",usuarioTO);
		modelAndView.setViewName("cargaArchivo/cargaArchivoInicio");
		return modelAndView;
	}

	@RequestMapping(value="cargaArchivo", method=RequestMethod.POST)
    public String uploadFile(@RequestParam("file") MultipartFile archivo,UsuarioTO usuarioTO) {
		logger.info("CargaArchivoController.uploadFile");
		logger.info("  Parametros de Peticion nombreArchivo: ["+archivo.getOriginalFilename()
						+"] type: ["+archivo.getContentType()
						+"] tamanio: ["+archivo.getSize()
						+"] region: ["+usuarioTO.getOp()+"]");
		ModelAndView modelAndView = new ModelAndView();
        if(!archivo.isEmpty()){
        	try {
				String nombreArchivo = archivo.getOriginalFilename().toLowerCase();
                if (nombreArchivo.endsWith(".xlsx")) {
                	Integer resultado = cargaArchivoService.cargaArchivoXlsx(archivo.getInputStream(),usuarioTO.getOp());
                	logger.info(" resultado: ["+resultado+"]");
                } else if(nombreArchivo.endsWith(".xls")) {
                	Integer resultado = cargaArchivoService.cargaArchivoXls(archivo.getInputStream(),usuarioTO.getOp());
                	logger.info(" resultado: ["+resultado+"]");
                } else {
                	logger.info("El archivo no es de excel");
                }
			} catch (IOException e) {
				logger.info("Error con el objeto archivo: "+e.getMessage());
			}
        }else{
        	logger.info("Archivo vacio");
        }
        modelAndView.addObject("usuarioTO",usuarioTO);
        logger.info("  * Fin Peticion");
        return "cargaArchivo/cargaArchivoCompletado";
    }

	@Autowired
	public void setCargaArchivoService(CargaArchivoService cargaArchivoService) {
		this.cargaArchivoService = cargaArchivoService;
	}

}
