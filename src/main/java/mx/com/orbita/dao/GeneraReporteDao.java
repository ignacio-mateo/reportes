package mx.com.orbita.dao;

import java.util.List;
import mx.com.orbita.to.ReporteTO;

public interface GeneraReporteDao {

	List<ReporteTO> getReporte(Integer idRegion);

}
