package mx.com.orbita.dao;

public interface CargaArchivoDao {

	Integer cargaArchivo(Long idSerie, Integer idRegion);
	Integer registraBitacora(String idSerie, Integer idRegion, String descripcion);
	void borrarSerie(Integer region);
	void borrarBitacora(Integer region);

}
