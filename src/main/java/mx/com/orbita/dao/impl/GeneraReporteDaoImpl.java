package mx.com.orbita.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import mx.com.orbita.dao.GeneraReporteDao;
import mx.com.orbita.to.ReporteTO;

@Component
public class GeneraReporteDaoImpl implements GeneraReporteDao {

	private JdbcTemplate jdbcTemplate;

	public static final String QUERY_REPORTE = "SELECT ID_SERIE, ID_REGION " +
			"FROM CARGA " +
			"WHERE ID_REGION = ?";

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<ReporteTO> getReporte(Integer idRegion) {
		List<ReporteTO> lista = new ArrayList<ReporteTO>();
		lista = jdbcTemplate.query(QUERY_REPORTE, new RowMapper(){
			public ReporteTO mapRow(ResultSet rs, int rowNum) throws SQLException {
				ReporteTO reporte = new ReporteTO();
				reporte.setIdSerie(rs.getInt(1));
				reporte.setIdRegion(rs.getInt(2));
				return reporte;
		}},idRegion);
		return lista;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

}
