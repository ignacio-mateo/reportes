package mx.com.orbita.dao.impl;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import mx.com.orbita.dao.CargaArchivoDao;

@Component
public class CargaArchivoDaoImpl implements CargaArchivoDao {

	private JdbcTemplate jdbcTemplate;

	public static final String INSERT_SERIE = "INSERT INTO CARGA VALUES(?,?)";

	public static final String INSERT_BITACORA = "INSERT INTO BITACORA VALUES(?,?,?)";

	public static final String DELETE_SERIE = "DELETE FROM CARGA WHERE ID_REGION = ?";

	public static final String DELETE_BITACORA = "DELETE FROM BITACORA WHERE ID_REGION = ?";

	public Integer cargaArchivo(Long idSerie, Integer idRegion) {
		return jdbcTemplate.update(INSERT_SERIE, idSerie, idRegion);
	}

	public Integer registraBitacora(String idSerie, Integer idRegion, String descripcion){
		return jdbcTemplate.update(INSERT_BITACORA, idSerie, idRegion, descripcion);
	}

	public void borrarSerie(Integer region){
		jdbcTemplate.update(DELETE_SERIE,region);
	}

	public void borrarBitacora(Integer region){
		jdbcTemplate.update(DELETE_BITACORA,region);
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

}
