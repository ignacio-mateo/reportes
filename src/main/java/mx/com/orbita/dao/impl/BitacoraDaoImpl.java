package mx.com.orbita.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import mx.com.orbita.dao.BitacoraDao;
import mx.com.orbita.to.BitacoraTO;

@Component
public class BitacoraDaoImpl implements BitacoraDao {

	private JdbcTemplate jdbcTemplate;

	private static final String QUERY_BITACORA = "SELECT ID_BITACORA, ID_REGION, DESCRIPCION " +
			"FROM BITACORA WHERE ID_REGION = ?";

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<BitacoraTO> getBitacora(Integer idRegion){
		List<BitacoraTO> lista = new ArrayList<BitacoraTO>();
		lista = jdbcTemplate.query(QUERY_BITACORA, new RowMapper(){
			public BitacoraTO mapRow(ResultSet rs, int rowNum) throws SQLException {
				BitacoraTO bitacora = new BitacoraTO();
				bitacora.setIdBitacora(rs.getString(1));
				bitacora.setIdRegion(rs.getString(2));
				bitacora.setDescripcion(rs.getString(3));
				return bitacora;
		}},idRegion);
		return lista;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

}
