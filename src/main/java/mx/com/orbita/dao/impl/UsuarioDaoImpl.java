package mx.com.orbita.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import mx.com.orbita.dao.UsuarioDao;
import mx.com.orbita.to.UsuarioTO;

@Component
public class UsuarioDaoImpl implements UsuarioDao{

	private static final Logger logger = Logger.getLogger(UsuarioDaoImpl.class);

	private JdbcTemplate jdbcTemplate;

	public static final String QUERY_USUARIO = "SELECT ID_USUARIO,ID_PERFIL,ID_ESTATUS,NOMBRE,APELLIDOP,APELLIDOM " +
												"FROM USUARIO WHERE ID_USUARIO = ?";

	public static final String QUERY_USUARIOS = "SELECT U.ID_USUARIO, U.NOMBRE, U.APELLIDOP, " +
												"U.APELLIDOM, P.DESCRIPCION, E.DESCRIPCION " +
												"FROM USUARIO U " +
												"INNER JOIN PERFIL P ON P.ID_PERFIL = U.ID_PERFIL " +
												"INNER JOIN ESTATUS E ON E.ID_ESTATUS = U.ID_ESTATUS";

	public static final String INSERT_USUARIO = "INSERT INTO USUARIO VALUES(?,?,?,?,?,?)";

	public static final String UPDATE_USUARIO = "UPDATE USUARIO " +
														"SET ID_PERFIL = ?, " +
														"ID_ESTATUS = ? " +
														"WHERE ID_USUARIO = ?";

	public UsuarioTO getUsuario(Integer usuario) {
		UsuarioTO datos = new UsuarioTO();
		try {
			datos = jdbcTemplate.queryForObject(QUERY_USUARIO, new RowMapper<UsuarioTO>(){
				public UsuarioTO mapRow(ResultSet rs, int rowNum) throws SQLException{
					UsuarioTO usuarioTO = new UsuarioTO();
					usuarioTO.setIdUsuario(rs.getInt(1));
					usuarioTO.setIdPerfil(rs.getInt(2));
					usuarioTO.setIdEstatus(rs.getInt(3));
					usuarioTO.setNombre(rs.getString(4));
					usuarioTO.setApellidoP(rs.getString(5));
					usuarioTO.setApellidoM(rs.getString(6));
					return usuarioTO;
				}
			}, usuario);
		} catch (EmptyResultDataAccessException e) {
			logger.info("  No existe empleado: ["+e.getMessage()+"]");
			datos.setIdUsuario(0);
		}
		return datos;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<UsuarioTO> getUsuario(){
		List<UsuarioTO> lista = new ArrayList<UsuarioTO>();
		lista = jdbcTemplate.query(QUERY_USUARIOS, new RowMapper(){
			public UsuarioTO mapRow(ResultSet rs, int rowNum) throws SQLException {
				UsuarioTO usuario = new UsuarioTO();
				usuario.setIdUsuario(rs.getInt(1));
				usuario.setNombre(rs.getString(2));
				usuario.setApellidoP(rs.getString(3));
				usuario.setApellidoM(rs.getString(4));
				usuario.setPerfil(rs.getString(5));
				usuario.setEstatus(rs.getString(6));
				return usuario;
		}});
		return lista;
	}

	public Integer setUsuario(UsuarioTO usuarioTO){
		Integer resultado = jdbcTemplate.update(INSERT_USUARIO,
				usuarioTO.getIdUsuario(),
				usuarioTO.getIdPerfil(),
				usuarioTO.getIdEstatus(),
				usuarioTO.getNombre(),
				usuarioTO.getApellidoP(),
				usuarioTO.getApellidoM());
		return resultado;
	}

	public Integer updateUsuario(UsuarioTO usuarioTO){
		Integer resultado = jdbcTemplate.update(UPDATE_USUARIO,
				usuarioTO.getIdPerfil(),
				usuarioTO.getIdEstatus(),
				usuarioTO.getIdUsuario());
		return resultado;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

}
