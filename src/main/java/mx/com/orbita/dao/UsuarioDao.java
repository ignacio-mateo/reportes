package mx.com.orbita.dao;

import java.util.List;
import mx.com.orbita.to.UsuarioTO;

public interface UsuarioDao {

	UsuarioTO getUsuario(Integer usuario);
	List<UsuarioTO> getUsuario();
	Integer setUsuario(UsuarioTO usuarioTO);
	Integer updateUsuario(UsuarioTO usuarioTO);

}
