package mx.com.orbita.dao;

import java.util.List;
import mx.com.orbita.to.BitacoraTO;

public interface BitacoraDao {

	List<BitacoraTO> getBitacora(Integer idRegion);

}
