package mx.com.orbita.util;

public class Codigo {

	public static final Integer ID_PERFIL_ADM = 1;
	public static final Integer ID_PERFIL_USR= 2;

	public static final Integer ID_USUARIO_ACTIVO = 1;
	public static final String MSG_USUARIO_ACTIVO = "Usuario activo";

	public static final Integer ID_USUARIO_INACTIVO = 2;
	public static final String MSG_USUARIO_INACTIVO = "Usuario inactivo";

	public static final Integer ID_USUARIO_ENCONTRADO = 0;
	public static final String MSG_USUARIO_ENCONTRADO = "Usuario no encontrado";

	public static final String MSG_USUARIO_AGREGADO = "Usuario agregado";
	public static final String MSG_USUARIO_ACTUALIZADO = "Usuario actualizado";


}
