-- Scripts para ejecutarse en PostgreSQL
CREATE TABLE CARGA(
		Id_Serie        numeric(20) NOT NULL,
		Id_Region		numeric(5) NOT NULL,
		PRIMARY KEY (Id_Serie));

CREATE TABLE BITACORA(
		Id_Bitacora		varchar(5) NOT NULL,
		Id_Region		numeric(5) NOT NULL,
		Descripcion		varchar(50));

CREATE TABLE PERFIL(
		Id_Perfil		numeric(10) NOT NULL,
		Descripcion		varchar(20),
		PRIMARY KEY (Id_Perfil));

CREATE TABLE ESTATUS(
		Id_Estatus		numeric(10) NOT NULL,
		Descripcion		varchar(20),
		PRIMARY KEY (Id_Estatus));

CREATE TABLE USUARIO(
		Id_Usuario		numeric(10) NOT NULL,
		Id_Perfil		numeric(5) NOT NULL,
		Id_Estatus		numeric(5) NOT NULL,
		Nombre			varchar(20),
		ApellidoP		varchar(20),
		ApellidoM       varchar(20),
		CONSTRAINT FK_USUARIO_PERFIL FOREIGN KEY (Id_Perfil) REFERENCES PERFIL (Id_Perfil),
		CONSTRAINT FK_USUARIO_ESTATUS FOREIGN KEY (Id_Estatus) REFERENCES ESTATUS (Id_Estatus),
		PRIMARY KEY (Id_Usuario));

INSERT INTO PERFIL VALUES(1,'ADMINISTRADOR');
INSERT INTO PERFIL VALUES(2,'USUARIO');

INSERT INTO ESTATUS VALUES(1,'ACTIVO')
INSERT INTO ESTATUS VALUES(2,'DESACTIVADO')

INSERT INTO USUARIO VALUES(1,1,1,'JORGE','ORTIZ','ROMAN');